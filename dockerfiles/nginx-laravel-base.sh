#!/bin/bash

function print_ok_message()
{
    COLOR_GREEN='\033[0;32m'
    NO_COLOR='\033[0m'
    printf "${COLOR_GREEN}[INFO]${NO_COLOR} $1\n"
}

function print_error_message()
{
    COLOR_RED='\033[0;31m'
    NO_COLOR='\033[0m'
    printf "${COLOR_RED}[ERROR]${NO_COLOR} $1\n"
}

function laravel_init()
{
    LARAVEL_DIR="$1"

    CURRENT_FOLDER="$PWD"

    print_ok_message "Lavarel site init for ${LARAVEL_DIR}"
    cd "$LARAVEL_DIR"

    # Composer dependencies install.
    if [[ -f composer.json && ! -d vendor ]]
    then
        composer install
    fi

    # Setting cache folder permissions.
    if [ -d bootstrap/cache ]
    then
        chown -R www-data:www-data bootstrap/cache
        chmod -R 775 bootstrap/cache

        # Error Illuminate/Console/Application.php:56
        # link: https://laracasts.com/discuss/channels/laravel/error-while-upgrading-to-laravel-54
        rm -rf bootstrap/cache/*.php
    fi

    # Setting storage folder permissions.
    if [ -d storage ]
    then
        chown -R www-data:www-data storage
        chmod -R 775 storage
    fi

    if [ -d vendor ]
    then

        # Gives permissions to vendor folder
        chmod -R 775 vendor

        # Cleans laravel project.
        php artisan config:clear && php artisan cache:clear && php artisan view:clear
    fi

    cd "$CURRENT_FOLDER"

    print_ok_message "Lavarel initialized ok!"
}

# Sets default timezone.
function set_timezone()
{
    if [ ! -z ${DEFAULT_TIMEZONE+x} ]
    then
        ln -snf "/usr/share/zoneinfo/$DEFAULT_TIMEZONE" /etc/localtime
        echo "$DEFAULT_TIMEZONE" > /etc/timezone
        dpkg-reconfigure --frontend noninteractive tzdata
    fi
}

# Composer Github Auth configiguration
function github_oauth_composer_config()
{
    if [ ! -z ${GITHUB_COMPOSER_AUTH+x} ]
    then
        print_ok_message "Composer Github Auth configiguration"
        composer config -g github-oauth.github.com $GITHUB_COMPOSER_AUTH
    fi
}

# PHP
function php_config_builder()
{
    PHPINI_CONTENT=""

    # "date.timezone" config.
    if [ ! -z ${DEFAULT_TIMEZONE+x} ]
    then
        PHPINI_CONTENT="${PHPINI_CONTENT}date.timezone=${DEFAULT_TIMEZONE}\n"
    fi

    # "display_errors" config.
    if [ ! -z ${PHP_DISPLAY_ERRORS+x} ]
    then
        PHPINI_CONTENT="${PHPINI_CONTENT}display_errors=${PHP_DISPLAY_ERRORS}\n"
    fi

    # "log_errors" config.
    if [ ! -z ${PHP_LOG_ERRORS+x} ]
    then
        PHPINI_CONTENT="${PHPINI_CONTENT}log_errors=${PHP_LOG_ERRORS}\n"
    fi

    # "memory_limit" config.
    if [ ! -z ${PHP_MEMORY_LIMIT+x} ]
    then
        PHPINI_CONTENT="${PHPINI_CONTENT}memory_limit=${PHP_MEMORY_LIMIT}\n"
    fi

    # "upload_max_filesize" config.
    if [ ! -z ${PHP_UPLOAD_MAX_FILESIZE+x} ]
    then
        PHPINI_CONTENT="${PHPINI_CONTENT}upload_max_filesize=${PHP_UPLOAD_MAX_FILESIZE}\n"
    fi

    # "display_errors" config.
    if [ ! -z ${PHP_POST_MAX_SIZE+x} ]
    then
        PHPINI_CONTENT="${PHPINI_CONTENT}display_errors=${PHP_POST_MAX_SIZE}\n"
    fi

    # "cgi.fix_pathinfo" config.
    PHPINI_CONTENT="${PHPINI_CONTENT}cgi.fix_pathinfo=0\n"

    # Creates the custom PHP ini file, along with its symbolic links.
    printf "$PHPINI_CONTENT" > /etc/php/7.2/conf/php-custom.ini
    cd /etc/php/7.2/conf
    if [ ! -f ../fpm/conf.d/30-php-custom.ini ]
    then
        ln -s "$PWD/php-custom.ini" ../fpm/conf.d/30-php-custom.ini
        ln -s "$PWD/php-custom.ini" ../cli/conf.d/30-php-custom.ini
    fi
}

# Creates custom config for PHP-FPM
function php_fpm_pool_config()
{
    PHPFPM_CONTENT=$(cat << EOF
[www]
listen.mode = 0660
catch_workers_output = yes
php_admin_value[error_log] = /var/log/fpm-php.www.log
php_admin_flag[log_errors] = on
clear_env = no
security.limit_extensions = .php

EOF
    )
    PHPFPM_FILENAME="/etc/php/7.2/fpm/pool.d/z00-docker.conf"
    printf "$PHPFPM_CONTENT" > "$PHPFPM_FILENAME"
    chmod 0644 "$PHPFPM_FILENAME"
}

# Executes nginx-laravel-base entrypoint routines.
function base_entrypoint_routines()
{
    print_ok_message "Running nginx-laravel-base entrypoint routines."
    set_timezone
    php_config_builder
    php_fpm_pool_config

    print_ok_message "Starts PHP-FPM service."
    /etc/init.d/php7.2-fpm start
    github_oauth_composer_config
}
