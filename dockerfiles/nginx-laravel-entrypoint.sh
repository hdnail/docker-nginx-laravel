#!/bin/bash

source /nginx-laravel-base.sh

# Running nginx-laravel-base entrypoint routines.
base_entrypoint_routines

if [ -d "/var/www/html" ]
then
    laravel_init "/var/www/html"
fi

exec "$@"
